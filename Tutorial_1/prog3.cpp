#include <iostream>

int main() {

	int max_rounds = 10;

	// For loops have three parts:
	// 1) variable declaration/initialization (int i = 1).
	// 2) loop condition (i <= max_rounds)
	// 3) update (done at end of loop) (++i)
	// The update at the end does NOT have to be ++i, it can be any expression
	for(int i = 1; i <= max_rounds; i++) {

		std::cout << "'for' loop round " << i << "." << std::endl;

	}

	// While loops check for a single condition to be met.
	int j = 1;
	while(j <= max_rounds) {

		std::cout << "'while' loop round " << j << std::endl;
		j++;

	}

	//Exercise: Write a function to print every odd number from 1 to N

	return 0;

}