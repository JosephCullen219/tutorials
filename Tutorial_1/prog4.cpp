
#include <iostream>
#include <ctype.h>

using namespace std;

int main() {

	// if statements only execute if their condition is true
	// note that == is used to check equality and = is used to assign values
	// The else executes if the the 'if' part did not

	int x = 5;

	if(x == 5) {
		cout << "x equals 5" << endl;
	} else {
		cout << "x does not equal 5" << endl;
	}

	int y = 0;

	//!= means does not equal

	if(y != 10) {
		cout << "y does not equal 10" << endl;
	} else {
		cout << "y equals 10" << endl;
	}

	/*There are many boolean operators which often show up in if statements:
	* '==' true if equal
	* '!=' true if not equal
	* '<', '>', '<=', '>=', Less than, greater than, less than or equal to, greater than or equal to 
	* '!', NOTs the expression 
	* '&&' Logical AND, (if both are true)
	* '||' Logical OR (if one or both are true)
	*/

	//x was declared above
	cout << "Enter a number: " << endl;
	cin >> x;
	if(x > 7 && x < 50) {
		cout << "x is between 7 and 50 (exclusive)" << endl;
	} else {
		cout << "x is below 7 or above 50 (inclusive)" << endl;
	}


	char c;
	cout << "Enter a character: " << endl;
	cin >> c;

	// We can chain ifs and else ifs with an else at the end together
	if(isalpha(c)) {
		cout << "You entered a letter" << endl;
	} else if(isdigit(c)) {
		cout << "You entered a number" << endl;
	} else {
		cout << "You entered a special character" << endl;
	}

	// Exercise: Write a function to take in 4 numbers (a1, a2, b1, b2)
	// The function should print "The average of a1,a2 is higher than the average of b1,b2"
	// The function should print "The average of a1,a2 is the same as the average of b1,b2"
	// The function should print "The average of a1,a2 is lower than the average of b1,b2"
}