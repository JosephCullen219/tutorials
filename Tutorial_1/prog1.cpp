/*
* This shows some C++ basic stuff
* Compile by typing 
* $ g++ prog1.cpp
* Run by typing
* $ ./a.out
*/

/* Comments can be written like this for a large block of comments*/
// Or like this for comments until the end of a line


#include <iostream> // A standard C++ library which allows normal input output

using namespace std;

// Function declaration
int add(int a, int b);
	
int main(int argc, char **argv) {
	std::cout << "Hello world" << std::endl; 
	// Because we are using namespace std we do not need to use std::
	cout << "More hello world" << endl; 


	int x, y; // Variable declaration

	cout << "Enter a number" << endl;
	cin >> x;

	cout << "Enter a number" << endl;
	cin >> y;
	cout << "The sum of " << x << " and " << y  << " is " << add(x,y) << endl;
	
	cout << "Enter 2 numbers" << endl;
	cin >> x >> y;
	cout << "The sum of " << x << " and " << y << " is " << add(x,y) << endl;

	cout << "Goodbye everyone :( " << endl;

	//Exercise: Write a function to convert Fahrenheit to Celsius
	// Formula is C = (F-32) * 5/9
	// Hint: use the double type instead of int if you don't want decimals

}

// Function implementation for add
int add(int a, int b) {

	return a + b;

}
