// # Preprocessor directives.
#include <iostream>
#define MAX_ARRAY_LENGTH 10

// Function declaration
int add(int a, int b);
void incrementValues(int length, int array[]);

using namespace std;

// Code starts running here
int main() {

	//C++ does not always initialize values to 0. This makes things faster, but
	//is a common source for bugs. Make sure to initialize values before using them
	int x, y; // Variable declaration
	int w,z;
	w = 11;
	z = 56;

	x = 5; //Declare starting values for x and y. 
	y = 10;

	cout << "x = " << x << endl << "y =  " << y << endl;

	cout << "Sum w and z: " << add(w,z) << endl;
	cout <<" Sum x and y " << add(x,y) << endl;


	int a[] = {1,2,3,4,5}; //Initialize an array of size 5




	/*Unlike in Java it's not as easy to get the length of an array.
	*There are a few workarounds to this problem:
	*Use a preprocessor directive for max size of array
	*Use an expression like: sizeof(a)/sizeof(a[0]) (This does not work in all cases)
	*Use a dynamically allocated array. (NOT SHOWN).This may show up in future workshop / Office hours
	*Use something other than an array (std::Vector) is a good example
	*/

	int aLength = sizeof(a)/sizeof(a[0]);
	cout << "len =5= " << aLength << endl;

	int b[MAX_ARRAY_LENGTH]; //Create a new array of MAX_ARRAY_LENGTH


	//I manually loaded a with integers from 1-5. Since b is MAX_ARRAY_LENGTH there should be a better way to load b
	//What if I want to load b with every multiple of 0 from 10 * MAX_ARRAY_LENGTH-1

	for(int i = 0; i < MAX_ARRAY_LENGTH; i++ ) {
		b[i] = 10 * i;
		cout << i << "th value of b is " << b[i] << endl;
	}



	//Increment all values of b using a function. Since we are using a pointer to b 
	//Changes to b in the function are the same as changes to b in main(). 
	cout << endl << "About to increment all values of by by 1: " << endl ;	

	incrementValues(MAX_ARRAY_LENGTH, b);
	for(int i = 0; i < MAX_ARRAY_LENGTH; i++ ) {
		cout << i << "th value of b is " << b[i] << endl;
	}

	/*Exercise: Create a 3rd array of size MAX_ARRAY_LENGTH called c. Load this array with 
	* multiples of 2 from 0 -> MAX_ARRAY_LENGTH (0,2,4,6,8,10,...). Print out all values
	* Finish loading this array and then create a 4th array. Set each nth value of this
	* array to be equal to the nth value of b  times the nth value of c.
	* print out these values. Implement printing all values in an array with a function
	*/
	//if b = {11, 21, 31, 41} c = {0,2,4,6} then d = {11*0,21*2,31*4,41*6}



}
void incrementValues(int length, int array[]) {
	for(int i = 0; i < length; i++) {
		array[i]++;
	}
}
// Function implementation for add
int add(int a, int b) {

	return a + b;

}