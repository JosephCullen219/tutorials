/*
* This shows the C++ primitive types
* Compile by typing 
* $ g++ prog2.cpp
* Run by typing
* $ ./a.out
*/

#include <iostream>
#include <string>
#include <stdio.h>


int main() {

	std::cout << std::endl <<
	"Variables are essentially 'boxes' that contain bytes. In C and C++," << std::endl <<
	"variables must be TYPED (i.e. they must be declared as some data type)." << std::endl <<
	"A Data Type defines how many bytes a box can hold, as well as how to" << std::endl <<
	"interpret the bytes." << std::endl <<
	std::endl;

	printf("This are the sizes of data types on my machine:\n");
	printf("\t'bool' is %lu byte.\n", sizeof(bool));
	printf("\t'char' is %lu byte.\n", sizeof(char));
	printf("\t'short' is %lu bytes.\n", sizeof(short));
	printf("\t'int' is %lu bytes.\n", sizeof(int));
	printf("\t'unsigned int' is %lu bytes.\n", sizeof(unsigned));
	printf("\t'long' is %lu bytes.\n", sizeof(long));
	printf("\t'long long' is %lu bytes.\n", sizeof(long long));
	printf("\t'float' is %lu bytes.\n", sizeof(float));
	printf("\t'double' is %lu bytes.\n", sizeof(double));
	printf("\t'long double' is %lu bytes.\n", sizeof(long double));
	printf("\t'pointer' is %lu bytes.\n", sizeof(void*));
	printf("\t'nullptr' is %lu bytes.\n", sizeof(NULL));

	std::string my_str("Hello!\n");

	std::cout << my_str <<
	"Variables must be DECLARED. This means you must state what type a variable\n" <<
	"will be before it is used.\n"
	<< std::endl;

	printf("This is how you declare a character:\n\tchar c;\n");
	printf("This is how you declare a double:\n\tdouble d;\n\n");
	
}