<// # Preprocessor directives.
#include <iostream>

// Function declaration
 void divideByValue(int a, int b);
 void divideByAlias(int &a, int b);

using namespace std;

// Code starts running here
int main() {	


	int a = 10;
	int &b = a;
	int c = a;

	cout << "Inital values a= " << a << " b= " << b << " c= " << c << endl;
	a++;
	cout << "After incrementing a, a= " << a << " b= " << b << " c= " << c << endl;
	b++;
	cout << "After incrementing b, a= " << a << " b= " << b << " c= " << c << endl;
	c++;
	cout << "After incrementing c, a= " << a << " b= " << b << " c= " << c << endl;
	cout << "Notice how any change to a or b affects the other. Any change to c does NOT affect a or b" << endl;
	


	/*In a language like Java you cannot directly change the value of a primitive
	* data type such as int from within a function. You can directly change the
	* values of an object from within a function because Java uses a concept similar
	* to pointers to allow this to happen. In C++ we can use pointers in more advanced
	* ways which can be a very powerful tool in some circumstances.
	*/
	int x = 256; // Variable declaration
	//Initalize x and print out value
	cout << "Inital value of x " << x << endl;

	

	//Notice how divideByValue() is a void function. It does not return anything.
	//When we pass a variable by value we are making a copy of the variable.
	//Any changes on the copy are not reflected in the original. After we call 
	//DivideByValue() x still has the same value as before.
	divideByValue(x,2);
	cout << "Second value of x " << x << endl;
	/* divideByAlias uses the '&' to indicate that the input for a is being treated
	* as an alias. This is equivalent to using a pointer. Thefore anything that happens
	* to the value in the function is happening to the same value in main()
	*
	*/
	divideByAlias(x,2);
	cout << "Third value of x " << x << endl;
	divideByAlias(x,2);
	cout << "fourth value of x " << x << endl;

	/*Exercise Write a function called swap. This function should accept 2 ints
	* as a parameter. This function should swap the values store in each of the
	* params. The function should use aliases so the values are actually swapped
	* outside the scope of the function.
	*/

	int i = 10;
	int j = 15;
	swap(i,j);


}

void divideByValue(int a, int b) {

	a = a / b;
	cout << "Value in divideByValue(): " << a << endl;
}

void divideByAlias(int &a, int b) {
	a = a / b;
	cout << "Value in divideByAlias(): " << a << endl;
}

