// # Preprocessor directives.
#include <iostream>
using namespace std;

// Code starts running here
int main() {


	

	namespace myMain{
	int x = 10;
}
	cout << x << endl;

	{
		int x = 15;
		cout << x << endl;
		using myMain::x;
		cout << x << endl;
	}
	cout << x << endl;

}